# nanonetbsd

# Content

It gives a minimum NetBSD environment, which is destined to run fsck or to install NetBSD from NetBSD or additional media with the sets.
This image is ideally suited for a memstick, which has at least 17-18 Megabytes.

# Architecture

AMD64 


# Release 

*Version 0* image with an installer and several disk utils.



